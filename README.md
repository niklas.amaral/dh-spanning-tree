# DH Spanning Tree

Eine Implementierung des Spanning Tree Netzwerkprotokolls in Lua

## Name
Spanning Tree Netzwerkprotokoll in Lua

## Description
In diesem Projekt wird das Spanning Tree Protokoll in der Programmiersprache Lua implementiert. Es kann eine Knoten-Kanten-Datei eingelesen werden auf dessen Grundlage ein Spanning Tree generiert wird. Die Knoten-Kantendatei kann sowohl manuell im Format XYZ erstellt werden, als auch mit Hilfe des Programms generiert werden. Der generierte Spanning Tree wird in einer Ausgabedatei gespeichert.

## Installation
Um dieses Programm zu starten, benötigt man sowohl Lua 5.4 als auch den Luarocks Paketemanager. Diese können mit Hilfe dieser Anleitungen installiert werden:

lua:
https://www.lua.org/start.html#installing

luarocks:
https://github.com/luarocks/luarocks/wiki/Download

Nachdem Lua und luarocks installiert wurden, wird die Erweiterung Luafilesystem benötigt.  
luarocks install luafilesystem

Auf Unix Geräten kann es dazu kommen, dass der Installationspfad noch in den PATH hinzugefügt werden muss.  
luarocks path >> ~/.bashrc

Das Programm wird mit dem Befehl lua main.lua im Terminal ausgeführt

## Grundlegende Funktionsweise des Programms
Das Programm besteht aus drei Unterprogrammen:
- Hauptmenü-Programm
- Knoten-Kanten-Dateierstellungsprogramm
- Spanning-Tree-Generatorprogramm

### Hauptmenü
Im Hauptmenü kann sich zwischen dem Knoten-Kanten-Dateierstellungsprogramm und dem Spanning-Tree-Generator entscheiden

### Knoten-Kanten-Dateierstellung
Im Knoten-Kanten-Dateierstellungsprogramm kann man sich per Kommandozeile eine Knoten-Kanten-Datei im richtigen Format erstellen lassen. Es finden **keine** Überprüfungen statt, ob der Input mit den Grundvereinbarungen übereinstimmt (keine doppelten Verbindungen, keine Knoten mit der selben ID, etc)

### Spanning-Tree-Generator
Beim Spanning-Tree-Generator wird zunächst eine Textdatei mit dem Dateinamen "einlesen.txt" geöffnet und eingelesen. Anhand dem Aufbau der Datei unterscheidet nun das Programm, ob eine Kante oder ein Knoten definiert wurde. Es finden auch hier **keine** Überprüfungen statt, ob der Input sich an die Grundvereinbarungen hält.
Die Reihenfolge  der Knoten- und Kantendefinitionen spielt keine Rolle. Die Knoten und Kanten werden in Tables gespeichert, die wenn sie mit Metatables modifiziert wurden sich ähnlich wie Objekte verhalten.

Wurden alle Knoten eingelesen, beginnt der Spanning-Tree-Algorithmus damit, sich einen zufälligen Knoten herrauszusuchen. Dieser Knoten sendet dann an alle Knoten, zu denen er eine Verbindung hat eine Nachricht mit dem Root den er kennt und den Kosten zu dem Root hin. Dies geschieht so lange, bis alle Knoten so oft besucht wurden, wie es Knoten gibt. Am Ende werden noch zwei explizite und ein impliziter Test durchgelaufen, bei denen darauf geschaut wird, ob jeder Knoten einen Root hat und ob jeder Knoten maximal einen Weg zum Root hat (Der Root Knoten ist nicht mit sich selbst verbunden). Dabei wird mitgeprüft, ob jeder Knoten im Spannbaum verbunden ist.

### Aufbau Knoten-Table
Ein Knoten hat folgenden Aufbau:

#### Call by Value
- Knotennamen
- KnotenID
- RootID
- Wegkosten zum Root
- Anzahl der Besuche

#### Call by Reference
- Kanten-Table (Wie Array)
- Self

#### Methoden
- print (Infos über Knoten)
- printVerbindungen (Infos über Verbindungen)
- shortPrint (Anzahl wie oft besucht)
- checkVerbindungen (Überprüft, ob gesendeter Rootvorschlag angenommen werden sollte)

## Authors and acknowledgment
Entwickler: Niklas J. Indorf (inf20010)

## License
MIT-License


