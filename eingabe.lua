--[[
    Lua Programm zur Erstellung einer Knoten-Kanten-Datei, welche eingelesen werden kann
]]
do
    require "lfs"

    local status = "1"
    local fehler = false

    local dateiPfad = lfs.currentdir() .. "/einlesen.txt"
    
    while status ~= "0" and status ~= "ende" do
        -- CMD leeren
        if not os.execute("clear") then
            os.execute("cls")
        end
        if fehler then
            print("Bitte treffen Sie eine gültige Auswahl\n")
        end

        print("Helferprogramm zur Erstellung einer Knoten-Kanten-Datei")
        print("\nWollen Sie eine neue Datei erstellen? 1. Ja oder 2. Nein")
        print("\nMit 0 oder ende verlassen Sie das Programm")
        status = io.read()

        if status == "1" or status == "Ja" then
            print(dateiPfad)
            local datei = io.open(dateiPfad, "w")
            print("\nBitte geben Sie einen Namen für den Graphen ein: ")
            local eingabe = io.read()
            eingabe = string.lower(eingabe)
            datei:write("graph " .. eingabe .. " {\n//Nodes\n")
            print("\n\nKnoteneingabe:")
            print("\nBitte geben Sie einen Namen eine ID für einen Knoten an.")
            print("\nGeben Sie 'ende' ein, um zur Kanteneingabe zu gelangen.")
            while eingabe ~= "ende" do
                print("\nKnotenname: ")
                eingabe = string.upper(io.read())
                if eingabe ~= "ENDE" then
                    datei:write(eingabe .. " = ")
                    print("\nKnoten-ID: ")
                    eingabe = io.read()
                    datei:write(eingabe .. ";\n")
                else
                    eingabe = string.lower(eingabe)
                end
            end
            eingabe = nil
            datei.write(datei, "//Links mit zugeh. Kosten\n")
            print("\n\nKanteneingabe:")
            print("\nBitte geben Sie einen zwei Knoten und die Pfadkosten zwischen diesen an.")
            print("\nGeben Sie 'ende' ein, um zum Ende zu gelangen.")
            while eingabe ~= "ende" do
                print("\n1. Knoten: ")
                eingabe = string.upper(io.read())
                if eingabe ~= "ENDE" then
                    datei:write(eingabe .. " - ")
                    print("\n2. Knoten: ")
                    eingabe = string.upper(io.read())
                    datei.write(datei, eingabe .. " : ")
                    print("\nKosten: ")
                    eingabe = io.read()
                    datei:write(eingabe .. ";\n")
                else
                    eingabe = string.lower(eingabe)
                end
            end
            datei:write("}")
            datei:close()
            fehler = false
            
        elseif status == "2" or status == "Nein" or status == "0" then
            fehler = false
        else
            fehler = true
        end
    end
end
