--[[
    Lua Programm zur Ausgabe einer Spanning-Tree-Datei
]]
do
    local Knoten = {}
    local KnotenMarker = " = "
    local KantenMarker = " : "

    local Datei = io.open("einlesen.txt")
    local Zeilen = Datei:lines()

    -- Funktionen
    function chooseRandomKnoten(keys, Knoten)
        local key = keys[math.random(#keys)]
        return Knoten[key]
    end

    function checkAllDone(Knoten)
        local AllDone = true
        local anzahl = 0
        
        for _, _ in pairs(Knoten) do anzahl = anzahl + 1 end

        --print(anzahl)

        for _, v in pairs(Knoten) do
            -- print(v)
            if v.besucht < anzahl then
                AllDone = false
                -- print(AllDone)
            end
        end
        return AllDone
    end

    function testAlleKnoten(Knoten)
        local rootAnzahl = 0
        local verbindungenZumRoot = 0
        for k, v in pairs(Knoten) do
            rootAnzahl = 0
            verbindungenZumRoot = 0

            if v.rootID ~= nil then rootAnzahl = 1 end

            for w, x in pairs(Knoten) do
                if x[2] == true then
                    verbindungenZumRoot = verbindungenZumRoot + 1
                end
            end
        end
        if verbindungenZumRoot > 1 then
            print("Error: Zu viele Verbindungen zum root\n")
        end

        if rootAnzahl ~= 1 then
            print("Error: Kein Root bei manchen Knoten\n")
        end

        if verbindungenZumRoot <= 1 and rootAnzahl == 1 then
            print("Success: Alle Tests erfolgreich durchgelaufen\n")
        end
    end

    function ausgabeAlleKnoten(Knoten)
        print("ID | Node | Root | Cost | Next Hop | PDU")
        for _, v in pairs(Knoten) do
            --    v:print()
            --    v:printVerbindungen()
            --    --socket.sleep(1)
            local nextHop = nil
            for w, x in pairs(v.verbindungen) do
                if x[2] == true then
                    nextHop = w
                end
            end

            print(string.format("%3s|%6s|%6s|%6s|%10s|%4s", v.id, v.name, v.rootID, v.rootKosten, nextHop, v.besucht))
            end
    end

    -- Wenn ein : oder ein = in einer Zeile gefunden wird, dann ist eine Definition vorhanden
    -- Lädt die Knoten-Kantendatei in den Arbeitsspeicher
    for Zeile in Zeilen do
        
        if Zeile:find(KnotenMarker)  then
            --print("\nKnoten " .. Zeile)
            -- Findet Beginn vom Knotenmarker
            local pos = Zeile:find(KnotenMarker)
            local tablePos = Zeile:sub(1, pos - 1)

            -- Neues Knotenobjekt wird erstellt
            Knoten[tablePos] = require("knoten"):new()

            -- Füllt Knotenobjekt mit Daten aus Textdatei
            Knoten[tablePos].name = tablePos
            Knoten[tablePos].id = Zeile:sub(pos + #KnotenMarker, #Zeile - 1)
            Knoten[tablePos].rootID = Knoten[tablePos].id

            --Knoten[tablePos]:print()

        elseif Zeile:find(KantenMarker) then
            --print("\nKante " .. Zeile)
            -- Findet Beginn vom Kantenmarker und dem Knotentrenner
            local Knotentrenner = " - "
            local pos = Zeile:find(KantenMarker)
            local posKnoten = Zeile:find(Knotentrenner)

            local knoten1 = Zeile:sub(1, posKnoten - 1)
            local knoten2 = Zeile:sub(posKnoten + #Knotentrenner, pos - 1)
            local kosten = Zeile:sub(pos + #KantenMarker, #Zeile - 1)

            if tonumber(kosten) > 0 then
                Knoten[knoten1].verbindungen[knoten2] = {kosten, false}
                Knoten[knoten2].verbindungen[knoten1] = {kosten, false}
            end

            --print(Knoten[knoten2].verbindungen[knoten1])
            --socket.sleep(1)
        end
    end

    Datei:close()

    -- Lädt alle Keys in eine getrennte Table
    local keys = {}
    for k, _ in pairs (Knoten) do
        table.insert(keys, k)
    end

    -- Test der RandomKnoten Funktion
    --[[
    for i = 1, 10, 1 do 
        local node = chooseRandomKnoten(keys, Knoten)
        node:print()
    end
    ]]

    while checkAllDone(Knoten) == false do
        local node = chooseRandomKnoten(keys, Knoten)

        for k, _ in pairs(node.verbindungen) do
        Knoten[k]:checkVerbindungen(node, node.rootID, node.rootKosten)
        --Knoten[k]:shortPrint()
        Knoten[k].besucht = Knoten[k].besucht + 1
        end
        --node:print()
    end

    --Schleife zum Testen von Methoden
    for k, v in pairs(Knoten) do
        v:print()
        v:printVerbindungen()
    --    --socket.sleep(1)
    end

    print("\nTests")
    testAlleKnoten(Knoten)

    print("\nEndausgabe")
    ausgabeAlleKnoten(Knoten)

    local answer
    repeat
        io.write("Weiter mit Eingabe... ")
        io.flush()
        answer=io.read()
    until answer ~= null
end