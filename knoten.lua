--[[
    Lua Modul für Knotenobjekte
]]

--Definierung eines "Objekts" "Node"
local node = {}
node.name = ""
node.id = ""
node.rootID = ""
node.rootKosten = "0"
node.besucht = 0


--[[Erstellung eines Konstruktors. 
Da Lua von sich aus keine Objekte unterstützt, arbeitet man
hier mit Hilfe von Metatables.]]
node.new = function (self, object)
    object = object or {}
    -- Verbindung nach dem Schema: Kosten, ist Weg zum Root
    object.verbindungen = {}
    setmetatable(object, self)
    self.__index = self
    return object
end

node.print = function (self)
    print("\nDieser Knoten heißt: " .. self.name .. " mit der ID: " .. self.id)
    print("Er kennt den Root mit der ID: " .. self.rootID .. " mit den Wegkosten: " .. self.rootKosten)
end


node.printVerbindungen = function (self)
    for k, v in pairs(self.verbindungen) do
        print("\n" .. self.name .. " - " .. k .. " kostet: " .. v[1])
        print("Ist Verbindung zum Root?: " .. tostring(v[2]))
    end
end

node.shortPrint = function (self)
    print("\n" .. self.name .. " - " .. "wurde " .. self.besucht .. ". Mal besucht.")
end

-- Prüft ob der vorgeschlagene Root angenommen wird
node.checkVerbindungen = function (self, fremdKnoten, proposedID, proposedKosten)
    proposedKosten = proposedKosten + fremdKnoten.verbindungen[self.name][1]

    -- Bei geringerer RootID wird der Vorschlag direkt übernommen
    if proposedID < self.rootID then
        for _, v in pairs(self.verbindungen) do
            v[2] = false
        end
        self.rootID = proposedID
        self.rootKosten = proposedKosten
        self.verbindungen[fremdKnoten.name][2] = true
    end

    -- Oder bei geringeren Kosten zur gleichen RootID
    if tonumber(proposedKosten) < tonumber(self.rootKosten) and tonumber(proposedID) == tonumber(self.rootID) then
        for _, v in pairs(self.verbindungen) do
            v[2] = false
        end
        self.verbindungen[fremdKnoten.name][2] = true
    end


end

return node