require "lfs"

do
    local eingabe = "1"
    local fehler = false
    while eingabe ~= "0" and eingabe ~= "ende" do
        --CMD wird bereinigt
        if not os.execute("clear") then
            os.execute("cls")
        end

        if fehler then
            print("Bitte treffen Sie eine gültige Auswahl\n")
        end

        --Abfrage Input oder Output erstellen
        print("Wollen Sie 1. eine Knoten-Kanten-Datei erstellen oder 2. einen Spanning-Tree erstellen?")
        print("\nMit 0 oder ende verlassen Sie das Programm")
        eingabe = io.read()

        --print(eingabe)
        --print(type(eingabe))
        
        if eingabe == "0" or eingabe == "1" or eingabe == "2" or eingabe == "ende" then
            if eingabe == "0" then
                --Mach nichts
            elseif eingabe == "1" then
                fehler = false
                dofile(lfs.currentdir().."/eingabe.lua")
            elseif eingabe == "2" then
                fehler = false
                dofile(lfs.currentdir().."/ausgabe.lua")
            elseif eingabe == "ende" then
                -- Mach nichts
            end
        else
            fehler = true
        end
    end
end
